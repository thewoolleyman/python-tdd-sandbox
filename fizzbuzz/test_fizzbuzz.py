# Requirements
#
# 1. Write a “fizzBuzz” method that accepts a number for input and returns it as a String.
#
# Notes:
#
# * start with the minimal failing solution
# * keep the three rules in mind and always write just sufficient code
# * do not forget to refactor your code after each passing test
# * write your assertions relating to the exact requirements
#
# 2. For multiples of three return “Fizz” instead of the number
#
# 3. For the multiples of five return “Buzz”
#
# 4. For numbers that are multiples of both three and five return “FizzBuzz”.
import fizzbuzzer


def test_fizzbuzz_requirement_1():
    assert fizzbuzzer.fizzbuzz(1) == "1"
    assert fizzbuzzer.fizzbuzz(13) == "13"
    assert fizzbuzzer.fizzbuzz(2) == "2"


def test_fizzbuzz_requirement_2():
    assert fizzbuzzer.fizzbuzz(3) == "Fizz"
    assert fizzbuzzer.fizzbuzz(6) == "Fizz"
    assert fizzbuzzer.fizzbuzz(9) == "Fizz"
    assert fizzbuzzer.fizzbuzz(-3) == "Fizz"


def test_fizzbuzz_requirement_3():
    assert fizzbuzzer.fizzbuzz(5) == "Buzz"
    assert fizzbuzzer.fizzbuzz(10) == "Buzz"
    assert fizzbuzzer.fizzbuzz(20) == "Buzz"
    assert fizzbuzzer.fizzbuzz(-10) == "Buzz"


def test_fizzbuzz_requirement_4():
    assert fizzbuzzer.fizzbuzz(0) == "FizzBuzz"
    assert fizzbuzzer.fizzbuzz(15) == "FizzBuzz"
