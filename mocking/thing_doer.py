def make_request(requests_api):
    response = requests_api.get("https://example.com")
    if response.status_code == 200:
        return response.content
    if response.status_code == 404:
        return False
