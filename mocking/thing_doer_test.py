import thing_doer


class FakeResponse:
    def __init__(self, content, status_code):
        self.content = content
        self.status_code = status_code

    def __call__(self, url):
        self.url = url
        return self


class FakeRequestsApi:
    def __init__(self, fake_response):
        self.fake_response = fake_response

    def get(self, url):
        self.actual_passed_url = url
        return self.fake_response


# https://martinfowler.com/bliki/TestDouble.html
# https://thoughtbot.com/blog/four-phase-test

def test_make_request_success():
    # setup
    fake_response = FakeResponse('fake content', 200)
    requests_api = FakeRequestsApi(fake_response)
    # exercise and verify
    assert thing_doer.make_request(requests_api) == 'fake content'


def test_make_request_failure():
    # setup
    fake_response = FakeResponse('failing content', 404)
    requests_api = FakeRequestsApi(fake_response)

    # exercise and verify
    assert thing_doer.make_request(requests_api) is False

def test_make_request_calls_example_dot_com():
    # setup
    fake_response = FakeResponse('fake content', 200)
    requests_api = FakeRequestsApi(fake_response)
    expected_url = "https://example.com"

    # exercise
    thing_doer.make_request(requests_api)

    # verify
    actual_passed_url = requests_api.actual_passed_url
    assert actual_passed_url == expected_url
